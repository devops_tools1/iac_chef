# Solucion del ejemplo 1

# 1. Crear una carpeta en el directorio home con el nombre ejemplo1
# Referencia: https://docs.chef.io/resources/directory/
directory '/home/ejemplo1' do
    owner 'root' # dejaremos a root como el usuario dueno de la carpeta
    group 'root' # el grupo lo dejaremos a root
    mode '0755' # colocaremos los permisos de la carpeta
    action :create # que accion deseamos ejecutar, en este caso queremos crear el directorio
end


# 2. En la carpeta creada se debde de existir un archivo de configuracion con extension .txt
# Referencia: https://docs.chef.io/resources/file/
file '/home/ejemplo1/conf.txt' do
    content 'configuracion: ejemplo1' # contenido de ejemplo
    mode '0755' # permisos del archivo
end

# 3. El servidor debe de tener el paquete de git instalado.
# Referencia: https://docs.chef.io/resources/apt_package/
apt_package 'git' do # por ser un sistema basado en debian (ubuntu 18.04) usaremos apt para instalar
    overwrite_config_files       true # sobreescribimos la configuracion de ser necesario
    package_name                 "git" # indicamos el paquete que deseamos instalar
    action                       :install # la accion que queremos realizar
end