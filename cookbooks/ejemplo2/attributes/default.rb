# definicion de los atributos que tendra el nodo y sus valores por defecto en caso
# no se especifiquen
default['directorio']['nombre'] = "default"
default['directorio']['permisos'] = "0755"
default["archivo"]["nombre"] = "default.txt"
default["archivo"]["contenido"] = "contenido desde atributos"
default["archivo"]["permisos"] = "0755"
default["estatico"]["nombre"] = "default.estatico.txt"
default["estatico"]["permisos"] = "0755"
# paquetes
default["paquetes"] = [
    "git",
    "wget"
]