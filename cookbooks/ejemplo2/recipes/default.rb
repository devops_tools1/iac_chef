# Solucion Ejemplo 2

# 1. Crear una carpeta en el directorio con las configuraciones pasadas de los atributos
# Referencia: https://docs.chef.io/resources/directory/
directory "/home/#{node['directorio']['nombre']}" do
    owner 'root' # dejaremos a root como el usuario dueno de la carpeta
    group 'root' # el grupo lo dejaremos a root
    mode "#{node['directorio']['permisos']}" # colocaremos los permisos de la carpeta
    action :create # que accion deseamos ejecutar, en este caso queremos crear el directorio
end

# 2. Se debe de crear un archivo en /home/ con configuraciones pasadas desde los atributos
# Referencia: https://docs.chef.io/resources/file/
file "/home/#{node['archivo']['nombre']}" do
    content "#{node["archivo"]["contenido"]}" # contenido de ejemplo
    mode "#{node["archivo"]["permisos"]}" # permisos del archivo
end

# 3. Se debe de crear un archivo estatico hacia la carpeta home del servidor
# Referencia: https://docs.chef.io/resources/cookbook_file/
cookbook_file "/home/#{node["estatico"]["nombre"]}" do
    source 'archivo_statico.txt' # nombre del archivo origen, debe de existir en la carpeta de files
    owner 'root'
    group 'root'
    mode "#{node["estatico"]["permisos"]}" # permissos del archivo.. se obtendran de los atributos
    action :create
end

# 4. Asegurarse poder instalar mas de solo un paquete en caso se necesite
# Refencia: https://docs.chef.io/resources/apt_package/
node["paquetes"].each do |paquete| # iterando en cada uno de los paquetes
    apt_package "#{paquete}" do # por ser un sistema basado en debian (ubuntu 18.04) usaremos apt para instalar
        overwrite_config_files       true # sobreescribimos la configuracion de ser necesario
        package_name                 "#{paquete}" # indicamos el paquete que deseamos instalar
        action                       :install # la accion que queremos realizar
    end
end