#! /bin/bash

docker build -t chefenv:v1 .

docker run -it --rm -v $PWD/cookbooks:/var/chef/cookbooks chefenv:v1 bash