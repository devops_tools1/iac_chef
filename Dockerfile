FROM ubuntu:18.04

RUN apt update

RUN apt upgrade

RUN apt install -y wget

RUN wget https://packages.chef.io/files/stable/chef-workstation/21.2.278/ubuntu/20.04/chef-workstation_21.2.278-1_amd64.deb

RUN dpkg -i chef-workstation_21.2.278-1_amd64.deb

RUN mkdir /var/chef

RUN mkdir /var/chef/cookbooks

RUN mkdir /var/chef_cache

COPY solo.rb /var/chef/solo.rb

WORKDIR /var/chef